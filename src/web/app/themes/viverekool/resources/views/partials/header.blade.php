<header class="banner">
  <nav class="nav-primary navbar navbar-expand-md navbar-light bg-faded bg-light">
    <div class="container justify-content-start align-items-center">
      <a class="brand navbar-brand order-0" href="{{ home_url('/') }}" title="{{ get_bloginfo('name', 'display') }}">
        <img src="@asset('images/8.png')" alt="{{ get_bloginfo('name', 'display') }}">
      </a>
      <div class="ml-auto pr-1 order-1 order-md-4">
        @include('partials.language-switcher')
      </div>
      <button class="navbar-toggler navbar-toggler-right order-2" type="button" data-toggle="collapse"
              data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
              aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse order-3" id="navbarSupportedContent">
        @if (has_nav_menu('primary_navigation'))
          {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'navbar-nav mr-auto']) !!}
        @endif
      </div>
    </div>
  </nav>
  @if (!empty($front_banner))
    {!! $front_banner !!}
  @endif
</header>

