<footer class="content-info mt-auto py-3">
  <div class="container">
    <div class="row align-items-center">
      <div class="col col-md-7 d-none d-md-block">
        <nav class="navbar navbar-expand navbar-dark">
          @if (has_nav_menu('footer_navigation'))
            {!! wp_nav_menu(['theme_location' => 'footer_navigation', 'menu_class' => 'navbar-nav']) !!}
          @endif
        </nav>
      </div>
      <div class="col col-md-5">
        @php dynamic_sidebar('sidebar-footer') @endphp
      </div>
    </div>
  </div>
  <div class="container text-center">
    Copyright &copy; {{ $copyright_data }} &mdash; Vivere erakool MTÜ. All rights reserved.
  </div>
</footer>
