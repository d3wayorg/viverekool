@if (function_exists('icl_get_languages'))
    @php
    $languages = apply_filters( 'wpml_active_languages', NULL, 'orderby=language_code&order=asc' );
    @endphp

    @if (!empty($languages) && count($languages) > 1)
    <div class="language-switcher">
        @foreach($languages as $language)
            @if (empty($language['active']))
                <a href="{{ $language['url'] }}">{{ $language['language_code'] }}</a>
            @else
                <span>{{ $language['language_code'] }}</span>
            @endif
        @endforeach
    </div>
    @endif
@endif
