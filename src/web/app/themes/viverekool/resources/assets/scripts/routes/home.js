function navbarFixed() {
  $('header .nav-primary').addClass('navbar-light fixed-top bg-light').removeClass('navbar-dark');
}

function navbarStatic() {
  $('header .nav-primary').removeClass('navbar-light fixed-top bg-light').addClass('navbar-dark');
}

export default {
  init() {
    if($(window).scrollTop() >= 50) {
      navbarFixed();
    } else {
      navbarStatic();
    }
    $(window).on('scroll', function() {
      const scrollTop = $(window).scrollTop();
      if(scrollTop >= 50) {
        navbarFixed();
      } else {
        navbarStatic();
      }
    });
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
