<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class FrontPage extends Controller
{
  public function frontBanner()
  {
    $html = get_field('front-banner');
    return empty($html) ? '' : '<div class="container banner-container">' . $html . '</div>';
  }

}
